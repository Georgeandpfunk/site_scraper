require 'rubygems'
require 'nokogiri'
require 'open-uri'

# Entry model
class Entry
  attr_reader :id
  attr_reader :title
  attr_reader :link
  attr_reader :summary
  attr_reader :source

  def initialize(title, link, summary, source)
    @title = title
    @link = link
    @summary = summary
    @source = source
  end

  def self.scrape(count, source)
    scrape_atts = ScrapeAttribute.find_by(source: source)
    doc = Nokogiri::HTML(open(scrape_atts[:url]))

    entries = doc.css(scrape_atts[:doc_split])
    entries_array = []
    entries[0, count.to_i].each do |entry|
      title = entry.css(scrape_atts[:title_split]).text
      link_base = entry.css(scrape_atts[:title_split])[0]['href'].to_s.gsub(scrape_atts[:link_sub], '')
      link_len = scrape_atts[:index_char].nil? ? link_base.length : link_base.index(scrape_atts[:index_char])
      link = link_base[0, link_len]
      summary = source == 'Vox' ? Summary.scrape(link, source) : entry.css(scrape_atts[:sum_split]).text

      entries_array << Entry.new(title, link, summary, source)
    end
    entries_array
  end
end

class Summary
  attr_reader :summary

  def initialize(summary)
    @summary = summary
  end

  def self.scrape(link, source)
    doc = Nokogiri::HTML(open(link))
    scrape_atts = ScrapeAttribute.find_by(source: source)
    entries = doc.css(scrape_atts[:sub_entry])
    @summary = entries.css(scrape_atts[:sub_summary]).text

    @summary
  end
end
