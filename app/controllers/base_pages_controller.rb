# Controller for the base pages of the app
class BasePagesController < ApplicationController
  def home; end

  def help; end

  def about; end

  def findthepiss
    @entries_array = []
    tinkle_source.each do |ts|
      @entries_array += Entry.scrape(tinkle_count, ts)
    end
    @entries_array.shuffle! # randomize the results
    @entries_array = @entries_array[0..(tinkle_count.to_i - 1)]
  end

  def displaythepiss; end

  private

  def tinkle_count
    return 10 if params[:search][:storycount].nil?
    params[:search][:storycount]
  end

  def tinkle_source
    params[:search][:storyloca].reject!(&:empty?)
  end

  # require/permit params here
end
