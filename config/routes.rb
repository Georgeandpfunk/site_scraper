Rails.application.routes.draw do
  root 'base_pages#home'
  get '/help', to: 'base_pages#help'
  get '/about', to: 'base_pages#about'
  get '/', to: 'base_pages#home'
  get  '/thepiss', to: 'base_pages#findthepiss', as: :show_the_piss
  post '/thepiss', to: 'base_pages#findthepiss', as: :find_the_piss
end
