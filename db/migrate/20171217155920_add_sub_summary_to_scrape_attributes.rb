# Add "sub_summary" column to scrape_attributes
class AddSubSummaryToScrapeAttributes < ActiveRecord::Migration[5.1]
  def change
    add_column :scrape_attributes, :sub_summary, :string
  end
end
