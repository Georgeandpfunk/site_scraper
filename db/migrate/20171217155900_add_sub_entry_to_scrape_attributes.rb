# Add "sub_entry" column to table scrape_attributes
class AddSubEntryToScrapeAttributes < ActiveRecord::Migration[5.1]
  def change
    add_column :scrape_attributes, :sub_entry, :string
  end
end
