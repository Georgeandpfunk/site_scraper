# Create table scrape_attributes
class CreateScrapeAttributes < ActiveRecord::Migration[5.1]
  def change
    create_table :scrape_attributes do |t|
      t.string :source
      t.string :url
      t.string :doc_split
      t.string :title_split
      t.string :link_sub
      t.string :sum_split
      t.string :index_char

      t.timestamps
    end
  end
end
