# Add index to "source" column in scrape_attributes
class AddIndexToScrapeAttributesSource < ActiveRecord::Migration[5.1]
  def change
    add_index :scrape_attributes, :source, unique: true
  end
end
